const cmd = require('node-cmd')

console.log('robot ready')
module.exports = robot => {

  robot.respond(/start frankie/i, res => {
    cmd.get(
      '"C:\\Program Files (x86)\\UiPath\\Studio\\UiRobot.exe" /file:"C:\\Users\\Fabian\\Documents\\UiPath\\LoopAsUsual\\Main.xaml"',
      (err, data, stderr) => {
        if(err) res.send('No se pudo levantar el robot '+err)
        res.send(data)
      })
  })

  robot.respond(/stop frankie/i, res => {
    cmd.get(
      'Taskkill /IM UiRobot.exe /F',
      (err, data, stderr) => {
        if(err) res.send('No se pudo parar el robot '+err)
        res.send(data)
      })
  })

}


